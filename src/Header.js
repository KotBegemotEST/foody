import { AppBar, Avatar, Badge, Container, IconButton, Menu, MenuItem, Toolbar, Tooltip } from "@mui/material";
import * as PropTypes from "prop-types";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { useContext, useState } from "react";
import { ArrowBack, ShoppingCart } from "@mui/icons-material";
import { useLocation, useNavigate } from "react-router-dom";
import { deepPurple } from "@mui/material/colors";
import { CartContext } from "./App";

function AdbIcon(props) {
    return null;
}

AdbIcon.propTypes = {
    sx: PropTypes.shape({
        mr: PropTypes.number,
        display: PropTypes.shape({ md: PropTypes.string, xs: PropTypes.string }),
    }),
};

const Header = (props) => {
    const navigate = useNavigate();
    const location = useLocation();
    console.log(location.pathname);
    const [anchorElUser, setAnchorElUser] = useState(null);

    const { cart } = useContext(CartContext);

    const totalAmount =
        []
            .concat(...Object.keys(cart).map((key) => cart[key]))
            .map((item) => item.count)
            .reduce((acc, curr) => acc + curr, 0) || 0;

    console.log({ totalAmount });

    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseUserMenu = (item) => {
        props?.handleItemClick?.(item);
        setAnchorElUser(null);
    };

    return (
        <AppBar style={{ backgroundColor: "rgba(80,200, 100,1)" }} position="static">
            <Container maxWidth="xl">
                <Toolbar style={{ justifyContent: "space-between" }}>
                    <Box style={{ display: "flex", alignItems: "center" }}>
                        {location.pathname !== "/restaurants" && (
                            <IconButton
                                onClick={() => navigate(-1)}
                                edge="start"
                                color="inherit"
                                aria-label="back"
                                sx={{ mr: 2 }}
                            >
                                <ArrowBack />
                            </IconButton>
                        )}
                        <AdbIcon sx={{ display: { xs: "none", md: "flex" }, mr: 1 }} />
                        <Typography
                            variant="h6"
                            noWrap
                            sx={{
                                mr: 2,
                                display: { xs: "none", md: "flex" },
                                fontFamily: "monospace",
                                fontWeight: 700,
                                letterSpacing: ".3rem",
                                color: "inherit",
                                textDecoration: "none",
                            }}
                        >
                            Foody
                        </Typography>
                    </Box>

                    <AdbIcon sx={{ display: { xs: "flex", md: "none" }, mr: 1 }} />
                    <Typography
                        variant="h5"
                        noWrap
                        component="a"
                        href=""
                        sx={{
                            mr: 2,
                            display: { xs: "flex", md: "none" },
                            flexGrow: 1,
                            fontFamily: "monospace",
                            fontWeight: 700,
                            letterSpacing: ".3rem",
                            color: "inherit",
                            textDecoration: "none",
                        }}
                    >
                        Foody
                    </Typography>

                    <Box style={{ display: "flex", alignItems: "flex-end" }}>
                        <Badge  onClick={() => navigate('/ShopingCart')} badgeContent={totalAmount} color="secondary" style={{ cursor:"pointer" }}>
                            <ShoppingCart color="action" />
                        </Badge>
                        <Box sx={{ flexGrow: 0 }} style={{ marginLeft: "20px" }}>
                            <Tooltip title="Open settings">
                                <IconButton id={"my-account"} onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                    <Avatar
                                        sx={{ bgcolor: deepPurple[500] }}
                                        alt={`${props.initials}`}
                                        src="/static/images/avatar/2.jpg"
                                    />
                                </IconButton>
                            </Tooltip>
                            <Menu
                                sx={{ mt: "45px" }}
                                id="menu-appbar"
                                anchorEl={anchorElUser}
                                anchorOrigin={{
                                    vertical: "top",
                                    horizontal: "right",
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: "top",
                                    horizontal: "right",
                                }}
                                open={Boolean(anchorElUser)}
                                onClose={handleCloseUserMenu}
                            >
                                {props.settings.map((setting) => (
                                    <MenuItem key={setting} onClick={handleCloseUserMenu.bind(this, setting)}>
                                        <Typography textAlign="center">{setting}</Typography>
                                    </MenuItem>
                                ))}
                            </Menu>
                        </Box>
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    );
};

export default Header;
