import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { Button, List, ListItem, ListItemText } from "@mui/material";
import Box from "@mui/material/Box";
import { ShoppingCartRounded } from "@mui/icons-material";
import Card from "@mui/material/Card";
import React, { useContext, useState } from "react";
import { CartContext } from "./App";

const ItemCard = (props) => {
    const { addToCart } = useContext(CartContext);
    const [counter, setCounter] = useState(1);

    return (
        <Card sx={{ maxWidth: 345 }}>
            <CardMedia component="img" alt={`${props.item.name}`} height="140" image={props.item.img} />
            <CardContent>
                <Typography style={{ minHeight: "70px" }} gutterBottom variant="h5" component="div">
                    {props.item.name}
                </Typography>
                <Typography
                    style={{
                        minHeight: "50px",
                        maxHeight: "100px",
                        overflow: "hidden",
                        marginBottom: "12px",
                    }}
                    gutterBottom
                    variant="p"
                    color="text.secondary"
                >
                    {props.item.desc}
                </Typography>
                <Typography gutterBottom variant="p" color="secondary">
                    Ingredients
                </Typography>
                <List style={{ maxHeight: "100px", overflow: "auto", marginBottom: "12px" }}>
                    {props.item?.ingredients?.split(",").map((i, index) => {
                        return (
                            <List key={i.id}>
                                <ListItem
                                    style={{ padding: "0 10px" }}
                                    divider={index !== props.item?.ingredients?.split(",").length - 1}
                                >
                                    <ListItemText style={{ textTransform: "capitalize" }} primary={i} />
                                </ListItem>
                            </List>
                        );
                    })}
                </List>
                <Typography gutterBottom variant="body3" color="text.secondary">
                    Price: €{counter * props.item.price}
                </Typography>
                <Box style={{ display: "flex", justifyContent: "space-between", marginTop: "8px" }}>
                    <Box style={{ display: "flex", alignItems: "center", marginRight: "10px" }}>
                        <Button
                            style={{ padding: "6px" }}
                            variant={"outlined"}
                            onClick={() => counter < 10 && setCounter(counter + 1)}
                        >
                            +
                        </Button>
                        <Typography style={{ margin: "0 10px" }} variant="p" color="secondary">
                            {counter}
                        </Typography>
                        <Button onClick={() => counter !== 1 && setCounter(counter - 1)} variant={"outlined"}>
                            -
                        </Button>
                    </Box>

                    <Button
                        onClick={() => {
                            addToCart(props.restId, props.item, counter);
                        }}
                        variant="contained"
                    >
                        <ShoppingCartRounded />
                    </Button>
                </Box>
            </CardContent>
        </Card>
    );
};

export default ItemCard;
