import { useParams } from "react-router-dom";
import { getDatabase, ref, child, get, onValue } from "firebase/database";
import React, { useState, useEffect, useCallback } from "react";
import { Button, Divider, IconButton, List, ListItem, ListItemText } from "@mui/material";
import { PlusOne, ShoppingCartRounded } from "@mui/icons-material";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import ItemCard from "./ItemCard";

const RestaurantMenu = () => {
    const params = useParams();

    const db = ref(getDatabase(), "Menus");
    const [data, setData] = useState([]);

    const fetchData = useCallback(async () => {
        await onValue(db, (snapshot) => {
            /*здесь вы можете полученные данные обработать*/
            const currentData = snapshot.val();
            console.log(currentData);
            const filtered = currentData
                .filter((i) => i.restaurantId === +params.restId)
                .map((item, idx) => ({ id: idx + 1, ...item }));
            setData(filtered);
        });
    }, []);

    useEffect(() => {
        fetchData().then((res) => console.log(data));
    }, [fetchData]); //only fetch rest when component mounts, otherwise we get infinite loop

    return (
        <Box sx={{ flexGrow: 1 }} style={{ padding: "20px" }}>
            {console.warn(data)}
            <Grid container rowSpacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
                {data.map((item, index) => (
                    <Grid item xs={6} sm={6} md={4} lg={3} key={item.id} gutterBottom>
                        <ItemCard
                            restId={+params.restId}
                            img={item.img}
                            title={item.name}
                            desc={item.desc}
                            price={item.price}
                            item={item}
                        />
                    </Grid>
                ))}
            </Grid>
        </Box>
    );
};

export default RestaurantMenu;
