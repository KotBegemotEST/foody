import {Navigate} from 'react-router-dom'
import {useAuthValue} from './AuthContext'

export default function PrivateRoute({children}) {
  const {currentUser} = useAuthValue()


  console.log('private route', currentUser)
  if(!currentUser?.emailVerified){
    return <Navigate to='/login' replace/>
  }

  return children
}
