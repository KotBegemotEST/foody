import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { ThemeProvider } from "@mui/material";
import { createTheme } from "@mui/material/styles";
import { deepPurple } from "@mui/material/colors";

const innerTheme = createTheme({
    palette: {
        primary: {
            light: "#757ce8",
            main: "rgba(80,200, 100,1)",
            dark: "#2b873b",
            contrastText: "#fff",
        },
        secondary: {
            main: deepPurple[500],
        },
    },
});

ReactDOM.render(
    <React.StrictMode>
        <ThemeProvider theme={innerTheme}>
            <App />
        </ThemeProvider>
    </React.StrictMode>,
    document.getElementById("root")
);

reportWebVitals();
