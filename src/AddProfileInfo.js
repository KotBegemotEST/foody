import { useAuthValue } from './AuthContext'
import { signOut } from 'firebase/auth'
import { getDatabase, ref, child, push, update,set,onValue } from "firebase/database";
import React, { useState, useEffect } from 'react';
import xtype from 'xtypejs'

import { auth } from './firebase'




function ProfileEdit() {
  const { currentUser } = useAuthValue();

  const db = getDatabase()
  const [data, setData] = useState([]) //состояние, в котором будут храниться данные.

  const [name, setName] = useState(null)
  const [phone, setPhone] = useState(null)
  const [payment, setPayment] = useState(null)
  const [address, setAddress] = useState(null)


  const handleInputChange = (e) => {
    const {id , value} = e.target;
    if(id === "name"){
      setName(value);
    }
    if(id === "address"){
      setAddress(value);
    }
    if(id === "phone"){
      setPhone(value);
      console.log("phone")
    }
    if(id === "credit"){
      setPayment(value);
      console.log("credit")
    }
    if(id === "cash"){
      console.log("cash")
      setPayment(value);
    }
}


const handleSubmit = () =>{
  const postData = {
    address: address,
    firstName: name,
    phone: phone,
    paymentByCard: payment,
    

  };

  const data = {}
  data["Users/" + currentUser?.uid] = postData
  console.log(data)
  update(ref(db), data)
}



  const Submit = () => {



     const postData = {
      address: address,
      firstName: name,
      paymentByCard: payment,
      phone: phone,
  
    };

    const data = {}
    data["Users/" + currentUser?.uid] = postData
    console.log(data)
    set(ref(db), data)
  }



  console.log(currentUser?.uid)



  return (


    <form className="profile-filling" >
      <input type="text" name={name} id="name" onChange = {(e) => handleInputChange(e)}  placeholder="Enter your  firstname" />
      <input type="text" name={address} id="address" onChange = {(e) => handleInputChange(e)} placeholder="Enter your  Address" />
      <input type="text" name={phone} id="phone"  onChange = {(e) => handleInputChange(e)} placeholder="Enter your  phoneNumber" />

      <div>
        <span>  Credit cart? </span>
        <input type="checkBox" name={"credit"} id="credit"  value="credit" onChange = {(e) => handleInputChange(e)} placeholder="Enter your  firstname" />
        <span>Cahs?</span>
        <input type="checkBox" name={"cash"} id="cash" value="cash"  onChange = {(e) => handleInputChange(e)} placeholder="Enter your  firstname" />
      </div>
      <span onClick={()=>handleSubmit()} type="submit">Submit</span>
    </form>
  )
}

export default ProfileEdit
