import "./App.css";
import { BrowserRouter as Router, Route, Navigate, Routes } from "react-router-dom";
import Profile from "./Profile";
import Restaraunts from "./Restaraunts";
import AccountBtn from "./AccountBtn";
import ProfileEdit from "./ProfileEdit";
import Register from "./Register";
import VerifyEmail from "./VerifyEmail";
import Login from "./Login";
import { useState, useEffect } from "react";
import { AuthProvider } from "./AuthContext";
import { auth } from "./firebase";
import { onAuthStateChanged } from "firebase/auth";
import PrivateRoute from "./PrivateRoute";
import ProfileMenu from "./ProfileMenu";
import { MenuItem } from "@mui/material";
import RestaurantMenu from "./RestarauntMenu";
import ShopingCart from "./ShopingCart";
import Header from "./Header";
import { CartProvider } from "react-use-cart";
import { createContext } from "react";

import { createTheme } from "@mui/material/styles";

const theme = createTheme({
    palette: {
        primary: {
            light: "#757ce8",
            main: "rgba(80,200, 100,1)",
            dark: "#002884",
            contrastText: "#fff",
        },
        secondary: {
            light: "#ff7961",
            main: "#f44336",
            dark: "#ba000d",
            contrastText: "#000",
        },
    },
});

export const CartContext = createContext({ cart: {}, addToCart: () => {}, removeFromCart: () => {} });

function App() {
    const [cart, setCart] = useState({});
    const [currentUser, setCurrentUser] = useState(null);
    const [timeActive, setTimeActive] = useState(false);

    const addToCart = (restId, item, count = 1) => {
        setCart((prev = {}) => {
            if (!prev[restId]) {
                return { ...prev, [restId]: [{ ...item, count }] };
            }
            if (prev[restId] && prev[restId].length === 0) {
                return { ...prev, [restId]: [{ ...item, count }] };
            }
            if (prev[restId] && !prev[restId].find((i) => i.id === item.id)) {
                return { ...prev, [restId]: [{ ...item, count }, ...prev[restId]] };
            }
            if (prev[restId] && prev[restId].find((i) => i.id === item.id)) {
                const filteredItems = prev[restId].filter((i) => i.id !== item.id);
                const currentItem = prev[restId].find((i) => i.id === item.id);
                return { ...prev, [restId]: [{ ...currentItem, count: currentItem.count + count }, ...filteredItems] };
            }
            return prev;
        });
    };

    const removeFromCart = (restId, itemId) => {
        setCart((prev = {}) => ({ ...prev, [restId]: prev[restId].filter((i) => i.id !== itemId) }));
    };

    useEffect(() => {
        onAuthStateChanged(auth, (user) => {
            setCurrentUser(user);
        });
    }, []);

    console.log({ contextCart: cart });

    console.log(currentUser, !!currentUser);
    return (
        <CartContext.Provider value={{ cart, addToCart, removeFromCart }}>
            <Router>
                <AuthProvider value={{ currentUser, timeActive, setTimeActive }}>
                    <Routes>
                        <Route
                            path="/restaurants"
                            element={[
                                <PrivateRoute>
                                    {" "}
                                    <AccountBtn />{" "}
                                </PrivateRoute>,
                                <Restaraunts currentUser={currentUser} />,
                            ]}
                        />
                        <Route
                            path={"/restaurants/:restId"}
                            element={[
                                <PrivateRoute>
                                    {" "}
                                    <AccountBtn />{" "}
                                </PrivateRoute>,
                                <CartProvider>
                                    {" "}
                                    , <RestaurantMenu />{" "}
                                    {/* ,<ShopingCart /> */}
                                </CartProvider>,
                            ]}
                        />
                        <Route path="/login" element={<Login />} />
                        <Route
                            path="/fillAcc"
                            element={[
                                <PrivateRoute>
                                    {" "}
                                    <AccountBtn />{" "}
                                </PrivateRoute>,
                                <ProfileEdit />,
                            ]}
                        />
                        <Route
                            path="/addAcc"
                            element={[
                                <PrivateRoute>
                                    {" "}
                                    <AccountBtn />{" "}
                                </PrivateRoute>,
                                <ProfileEdit />,
                            ]}
                        />
                        <Route path="/register" element={<Register />} />
                        <Route path="/verify-email" element={<VerifyEmail />} />
                        <Route exact path="/" element={<Navigate to={"/restaurants"} />} />





                        <Route exact path="ShopingCart" element={[                        <PrivateRoute>
                                    {" "}
                                    <AccountBtn />{" "}
                                </PrivateRoute>,<ShopingCart />]} />
                    </Routes>
                </AuthProvider>
            </Router>
        </CartContext.Provider>
    );
}

export default App;
