import {useState} from 'react'
import { Link } from 'react-router-dom'
import './forms.css'
import {signInWithEmailAndPassword, sendEmailVerification} from 'firebase/auth'
import {auth} from './firebase'
import {useNavigate} from 'react-router-dom'
import {useAuthValue} from './AuthContext'
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';

const UIButton = styled(Button)({backgroundColor:"#3F51B5"})


function Login(){

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState('')
  const {setTimeActive} = useAuthValue()
  const navigate = useNavigate()

  const login = e => {
    e.preventDefault()
    signInWithEmailAndPassword(auth, email, password)
    .then(() => {
      if(!auth.currentUser.emailVerified) {
        sendEmailVerification(auth.currentUser)
        .then(() => {
          setTimeActive(true)
          navigate('/verify-email')
        })
      .catch(err => alert(err.message))
    }else{
      navigate('/restaurants')
    }
    })
    .catch(err => setError(err.message))
  }

  return(
    <div className='center main-page__image'>
      <div className={'main-page__gradient'} />
      <div className='auth'>
        <h1>Log in</h1>
        {error && <div className='auth__error'>{error}</div>}
        <form onSubmit={login} name='login_form'>
          <TextField
            id={"email"}
            label="Email"
            variant="outlined"
            type='email'
            value={email}
            placeholder="Enter your email"
            onChange={e => setEmail(e.target.value)}/>

          <TextField
              id={"password"}
            label="Password"
              variant="outlined"
            type='password'
            value={password}
            placeholder='Enter your password'
            onChange={e => setPassword(e.target.value)}/>

          <Button variant="contained" color="success" type={'submit'}>Login</Button>
        </form>
        <p>
          Don't have an account?
          <Link to='/register'>Create one here</Link>
        </p>
      </div>
    </div>
  )
}

export default Login
