import { useAuthValue } from './AuthContext'
import {onAuthStateChanged, signOut} from 'firebase/auth'
import { getDatabase, ref, child, push, update,set,onValue } from "firebase/database";
import React, { useState, useEffect } from 'react';
import xtype from 'xtypejs'

import { auth } from './firebase'
import {
  Alert,
  Box,
  Button, Divider,
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  Snackbar,
  TextField
} from "@mui/material";
import Typography from "@mui/material/Typography";
import {Edit} from "@mui/icons-material";




function ProfileEdit() {
  const { currentUser } = useAuthValue();

  const db = getDatabase()

  const userData = ref(getDatabase(), "Users/"+currentUser?.uid+"");

  const [name, setName] = useState(null)
  const [phone, setPhone] = useState(null)
  const [payment, setPayment] = useState(null)
  const [address, setAddress] = useState(null)
  const [editAccount, setEditAccount] = useState(false);
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    onValue(userData, (snapshot) => {
      /*здесь вы можете полученные данные обработать*/
      const currentData = snapshot.val();
      setName(currentData.firstName);
      setAddress(currentData.address);
      setPhone(currentData.phone);
      setPayment(currentData.paymentByCard);
    });
  }, [])

  const handleInputChange = (e) => {
    console.log(e.target.id, e.target.value, e);
    const {id, name , value} = e.target;
    if(id === "name"){
      setName(value);
    }
    if(id === "address"){
      setAddress(value);
    }
    if(id === "phone"){
      setPhone(value);
      console.log("phone")
    }
    if(name === "credit"){
      setPayment(value);
    }
    if(name === "cash"){
      setPayment(value);
    }
}


const handleSubmit = () =>{
  const postData = {
    address: address,
    firstName: name,
    phone: phone,
    paymentByCard: payment,
  };

  const data = {}
  data["Users/" + currentUser?.uid] = postData
  console.log(data)
  update(ref(db), data)
      .finally(() => setEditAccount(false))

}

  const Submit = () => {

     const postData = {
      address: address,
      firstName: name,
      paymentByCard: payment,
      phone: phone,

    };

    const data = {}
    data["Users/" + currentUser?.uid] = postData
    update(ref(db), data)
  }

  return (
      <div style={{
        padding: '10px 20px'
      }}>
        {
          !editAccount &&
            <>
              <Typography id={"welcome-text"} variant="h4" gutterBottom component="h4">
                {`Welcome, ${!!name ? name : 'dear user'}!`}
              </Typography>
              <Box style={{position: 'relative'}}>
                {
                    !!address && <Typography variant="subtitle2" gutterBottom component="p">
                      <b>Your delivery address</b>: {address}
                    </Typography>
                }
                {
                    !!phone && <Typography variant="subtitle2" gutterBottom component="p">
                      <b>Your phone number</b>: {phone}
                    </Typography>
                }
                {!!payment && <Typography variant="subtitle2" gutterBottom component="p">
                  <b>Payment method</b>: {payment}
                </Typography>}
                <Button
                    id={"edit-btn"}
                    style={{
                      position: 'absolute',
                      top: 0,
                      right: 0
                    }}
                    variant="outlined"
                    size="small"
                    endIcon={<Edit />}
                    onClick={() => setEditAccount(true)}>
                  Edit
                </Button>
              </Box>
              <>
                <Divider />
                <Typography variant="h4" gutterBottom component="h4">
                  {`Your recent orders`}
                </Typography>
                {!orders.length && <Typography variant="subtitle2" gutterBottom component="p">
                 Seems that you haven't order anything yet!
                </Typography>}
              </>

            </>


        }
        {
            editAccount && <>
              <Box
                component="form"
                sx={{
                '& .MuiTextField-root': { m: 1, width: '25ch' },
              }}
                noValidate
                autoComplete="off"
                className="profile-filling" >
                <FormControl fullWidth={true}>
                  <TextField
                      variant="outlined"
                      label="Your name"
                      type="text"
                      value={name}
                      id={"name"}
                      name={name}
                      onChange = {(e) => handleInputChange(e)} />
                </FormControl>
                <FormControl fullWidth={true}>
                  <TextField
                      fullWidth={true}
                      variant="outlined"
                      label="Your address"
                      type="text"
                      value={address}
                      name={address}
                      id={"address"} onChange = {(e) => handleInputChange(e)}/>
                </FormControl>
                <FormControl fullWidth={true}>
                  <TextField
                      fullWidth={true}
                      variant="outlined"
                      label="Your phone number"
                      type="tel"
                      value={phone}
                      name={phone}
                      id={"phone"}
                      onChange = {(e) => handleInputChange(e)} />
                </FormControl>
                <FormControl fullWidth={true}>
                  <FormLabel id="demo-controlled-radio-buttons-group">Payment method</FormLabel>
                  <RadioGroup
                      aria-labelledby="demo-controlled-radio-buttons-group"
                      name="controlled-radio-buttons-group"
                      value={payment}
                      onChange={(e) => handleInputChange(e)}
                  >
                    <FormControlLabel
                        id={'credit'}
                        name={'credit'}
                        value="credit"
                        control={<Radio />} label="Credit card" />
                    <FormControlLabel
                        id={'cash'}
                        name={'cash'}
                        value="cash"
                        control={<Radio />} label="Cash" />
                  </RadioGroup>
                </FormControl>
                <Button id={"save-btn"} variant="outlined" onClick={()=>handleSubmit()}>Save changes</Button>
              </Box>
            </>
        }
      </div>
  )
}

export default ProfileEdit
