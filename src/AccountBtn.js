import './profile.css'
import {useAuthValue} from './AuthContext'
import Header from "./Header";
import React from "react";
import { useNavigate } from 'react-router-dom';
import { getAuth, signOut } from "firebase/auth";


function Profile() {
  const {currentUser} = useAuthValue();
  const navigate = useNavigate();

  const handleItemClick = (item) => {
    console.log(item);
    switch (item) {
      case 'Account':
        navigate("/fillAcc");
        return;
      case 'Sign out':
        const auth = getAuth();
        signOut(auth).then(() => {
          console.log('signed out');
          navigate("/login");
        }).catch((error) => {
          console.log(error);
        });
        return;
      default:
        navigate("/restaurants");
        return;
    }

  }

  return(!!currentUser ?  <Header handleItemClick={handleItemClick.bind(this)} settings={['Account', 'Sign out']} initials={currentUser?.email?.charAt(0).toUpperCase()}/> : <></>)
}

export default Profile
