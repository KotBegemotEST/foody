import React, {useState,useEffect} from 'react';
import './profile.css'
import './Profile.js'
import { getDatabase, ref, child, get,onValue } from "firebase/database";
import './Restaraunts.css'
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { experimentalStyled as styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import {Button, Link} from "@mui/material";
import {useNavigate} from "react-router-dom";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(2),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));
// import Profile from './Profile';




function Restaraunts(props){
  const navigate = useNavigate();
  const db = ref(getDatabase(), "Restaurants/"); // ссылку лучше вынести в отдельный API, либо сделать кастомный хук.
  const [data, setData] = useState([]) //состояние, в котором будут храниться данные.



  useEffect(()=>{ // fetch запросы рекомендуется выполнять в хуке useEffeck
    onValue(db, (snapshot) => {
      /*здесь вы можете полученные данные обработать*/
      const currentData = snapshot.val();
      // console.log(currentData) // для наглядности
      setData(currentData);
    });
}, []) //only fetch rest when component mounts, otherwise we get infinite loop


  let list = []
  for( let i =0;i<=data.length;i++){
    if (data[i] === undefined){
      continue
    }
    else{
      list.push(data[i])
    }
  }


  console.log(list);
  return(

        // <div className="restaraunts">
          <Box sx={{ flexGrow: 1 }} style={{padding: '20px'}}>
            <Grid container rowSpacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
              {list.map((restaraunt, index) => (
                  <Grid item xs={6} sm={6} md={4} lg={3} key={index} gutterBottom>
                    <Card id={`restaurantCart_${index}`} key={restaraunt.name} sx={{ maxWidth: 345 }}>
                      <CardMedia
                          component="img"
                          alt={`${restaraunt.name}`}
                          height="140"
                          image = {restaraunt.img}
                      />
                      <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                          {restaraunt.name}
                        </Typography>
                        <Typography style={{
                          minHeight: '100px',
                          maxHeight: '120px', overflow: 'hidden', marginBottom: '12px'}} gutterBottom variant="p" color="text.secondary">
                          {restaraunt.desc}
                        </Typography>
                        <Typography gutterBottom variant="body3" color="text.secondary">
                          Price level: {restaraunt.price}
                        </Typography>
                        <Typography gutterBottom variant="body3" color="text.secondary">
                          Average Time: {restaraunt.avgTime}
                        </Typography>
                        <Button id={`viewMoreInfo_${index}`} style={{marginTop: '10px'}} variant={'outlined'} onClick={() => navigate(`/restaurants/${index+1}`)} size="large">View more</Button>
                      </CardContent>
                    </Card>
                  </Grid>
              ))}
            </Grid>

          </Box>
        // </div>


  )
}

export default Restaraunts
